import * as bcrypt from 'bcrypt'
import { check } from 'express-validator'
import { User } from '../models/user'

export const userRules = {
  forRegister: [
    check('email')
      .isEmail().withMessage('Formato email inválido'),
      check('rut')
      .isLength({ min: 1 }).withMessage('Invalid rut')
      .custom(rut => User.findOne({ where: { rut } }).then(u => !!!u)).withMessage('Ya existe un usuario con ese RUT'),
    check('password')
      .isLength({ min: 8 }).withMessage('Invalid password'),
    check('confirmPassword')
      .custom((confirmPassword, { req }) => req.body.password === confirmPassword).withMessage('Las contraseñas deben ser iguales'),
    check('name')
    .isLength({ min: 3 }).withMessage('Invalid name'),
  ],
  forLogin: [
    check('rut')
    .not().isEmpty().withMessage('Usuario no ingresado')
      .custom(rut => User.findOne({ where: { rut } }).then(u => !!u)).withMessage('Usuario invalido'),
    check('password')
  ]
}