import * as bcrypt from 'bcrypt'
import { check } from 'express-validator'
import { Tx } from '../models/tx'
import { User } from '../models/user'

export const txRules = {
  forDeposit: [
    check('amount')
      .isInt().withMessage('Invalid amount')
      .custom((amount) =>amount>0).withMessage('El monto debe ser mayor a 0'),
      check('description')

  ],
  forWithdraw:[
    check('amount')
    .isInt().withMessage('Invalid amount')
    .custom((amount) =>amount>0).withMessage('El monto debe ser mayor a 0')
    .custom((amount,{ req }) =>{
     
      return Tx.sum('amount',{where:{idUser:req.body.AuthUser.rut}}).then(currentAmount=>{
        if(currentAmount<amount){
          throw new Error('No tiene fondos para retirar esa cantidad. Saldo actual: '+currentAmount)
          
        }
      })

    }),
    check('description')    
  ],
  forTransfer:[
    check('amount')
    .isInt().withMessage('Invalid amount')
    .custom((amount) =>amount>0).withMessage('El monto debe ser mayor a 0')
    .custom((amount,{ req }) =>{
     
      return Tx.sum('amount',{where:{idUser:req.body.AuthUser.rut}}).then(currentAmount=>{
        if(currentAmount<amount){
          throw new Error('No tiene fondos suficientes para esta tranferencia. Saldo actual: '+currentAmount)
          
        }
      })

    }),
    check('toUser')
    .not().isEmpty().withMessage('Rut Invalido')
    .custom(rut => User.findOne({ where: { rut } }).then(u => {if(!u)throw new Error('Usuario destino no existe')})),
    check('description')

  ]
}