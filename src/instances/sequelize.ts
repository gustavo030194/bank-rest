import {Sequelize}  from 'sequelize'


const db = process.env.DB_NAME || 'ripley'
const username = process.env.DB_USER ||'ripley'
const password = process.env.DB_PASSWORD || 'Ripley123'

export const sequelize = new Sequelize(db, username, password, {
  host:process.env.DB_HOST || 'localhost',
  dialect: "postgres",
  port: 5432,
});
sequelize.sync();
sequelize.authenticate()