import { Router } from 'express'
import { matchedData,validationResult } from 'express-validator'
import { txRules } from '../rules/tx.rules'
import { TxService } from '../services/tx.service'
import { TxAddModel } from '../models/tx'

export const txRouter = Router()
const txService = new TxService()

txRouter.post('/deposit', txRules['forDeposit'], (req, res) => {
    const errors = validationResult(req)
    
    if (!errors.isEmpty())
        return res.status(422).json(errors.array())

    const payload = matchedData(req) as TxAddModel
    let user = req.body.AuthUser;

    const tx = txService.deposit(payload,user)
    return tx.then(t => res.json(t))
})

txRouter.post('/withdraw', txRules['forWithdraw'], (req, res) => {
    const errors = validationResult(req)

    if (!errors.isEmpty())
        return res.status(422).json(errors.array())

    const payload = matchedData(req) as TxAddModel
    let user = req.body.AuthUser;
    const tx = txService.withdraw(payload,user)

    return tx.then(t => res.json(t))
})


txRouter.post('/transfer', txRules['forTransfer'], (req, res) => {
    const errors = validationResult(req)

    if (!errors.isEmpty())
        return res.status(422).json(errors.array())

    const payload = matchedData(req) as TxAddModel
    let user = req.body.AuthUser;
    const tx = txService.transfer(payload,user)

    return tx.then(t => res.json(t))
})

txRouter.get('/activity',(req, res) => {
    const errors = validationResult(req)

    if (!errors.isEmpty())
        return res.status(422).json(errors.array())
        let user = req.body.AuthUser;
    const activities = txService.getActivities(user)

    return activities.then(t => res.json(t))
})

txRouter.get('/summary',(req, res) => {
    const errors = validationResult(req)

    if (!errors.isEmpty())
        return res.status(422).json(errors.array())
        let user = req.body.AuthUser;
    const activities = txService.getSummary(user)

    return activities.then(t => res.json(t))
})