
import * as bcrypt from 'bcrypt'
import * as jwt from 'jsonwebtoken'
import {Tx, TxModel, TxAddModel, TxViewModel } from '../models/tx'
import {Op} from 'sequelize';
import { UserModel } from '../models/user'

export class TxService {


    static get txAttributes() {
        return ['id','type','description','amount']
    }
    private static _tx
    static get tx() {
        return TxService._tx
    }

    deposit({amount,description}: TxAddModel,user:UserModel) {
        let idUser = user.rut;
        let type ='DEPOSIT'
        return Tx.create({type,amount,description,idUser})
        .then(t => this.getTxById(t!.id))

    }

    withdraw({amount,description}: TxAddModel,user:UserModel) {
        let idUser = user.rut;
        let newAmount = parseInt(amount)*-1;
        let type ='WITHDRAW';
        return Tx.create({type,amount:newAmount,description,idUser})
        .then(t => this.getTxById(t!.id))

    }
    async transfer({amount,description,toUser}: TxAddModel,user:UserModel) {
        let idUser = user.rut;
        let type ='TRANSFER';
           

        let transfer = await Tx.create({type,amount:-amount,description,idUser,toUser})
        .then(t => this.getTxById(t!.id))

        await Tx.create({type,amount,description,idUser:toUser})
        .then(t => console.log('Destination money Recived'));

        return  this.getTxById(transfer.id);

    }
    async getActivities(user:UserModel) {


        return Tx.findAll({
            where: {
              [Op.or]: [
                { idUser: user.rut},
              ]
            },order: [['createdAt','DESC']]});




    }

    getSummary(user:UserModel) {

        return Tx.sum('amount',{
            where: {
              [Op.or]: [
                { idUser: user.rut},
              ]
            }});

    }

    getTxById(id: number) {
        return Tx.findByPk(id, {
            attributes: TxService.txAttributes
        })
    }

 
}