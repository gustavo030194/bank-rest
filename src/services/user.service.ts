
import * as bcrypt from 'bcrypt'
import * as jwt from 'jsonwebtoken'
import { User, UserModel, UserAddModel, UserViewModel } from '../models/user'

export class UserService {
    private readonly _saltRounds = 12
    private readonly _jwtSecret = 'Ripley!12345'

    static get userAttributes() {
        return ['id','name','rut','email']
    }
    private static _user
    static get user() {
        return UserService._user
    }

    register({ email, password,rut,name }: UserAddModel) {
        return bcrypt.hash(password, this._saltRounds)
            .then(hash => {
                return User.create({email,rut,name, password: hash })
                    .then(u => this.getUserById(u!.id))
            })
    }

    login({ rut }: UserAddModel) {
        return User.findOne({ where: { rut } }).then(u => {
            const { id, rut } = u
            return { user:u,token: jwt.sign({ id, rut }, this._jwtSecret,{ expiresIn: 60*60 })}
        })
    }

    verifyToken(token: string) {
        return new Promise((resolve, reject) => {
            jwt.verify(token, this._jwtSecret, (err, decoded) => {
                if (err) {
                    resolve(null)
                    return
                }

                UserService._user = User.findByPk(decoded['id'])
                resolve(UserService._user)
                return
            })
        }) as Promise<UserViewModel>
    }

    getUserById(id: number) {
        return User.findByPk(id, {
            attributes: UserService.userAttributes
        })
    }
}