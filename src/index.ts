// src/index.ts

import * as express from 'express'
import * as cors from 'cors'
import * as bodyParser from 'body-parser'
import { userRouter } from './routers/user.router'
import { txRouter } from './routers/tx.router'
import { tokenGuard } from './middlewares/token-guard'
require('dotenv').config()


const app = express()
const port = 3000
const rootPath = process.env.ROOT_PATH || ''
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
app.use(rootPath+'/users/', userRouter)


// Unprotected Get
app.get(rootPath+'/public', (req, res, next) => {
    res.json('Hello World')
})
app.use(tokenGuard())
app.use(rootPath+'/', txRouter)
// Protected Get
app.get(rootPath+'/protected', (req, res, next) => {
    console.log('protected')
    let user = req.body.AuthUser;
    res.json(user)
})
app.listen(port, () => {
    console.log(`App is listening on port ${port}`)
})