import * as Sequelize from 'sequelize'
import { sequelize } from '../instances/sequelize'


export interface UserModel extends Sequelize.Model{
    id: number
    email: string
    password: string
    name: string
    rut: string
    createdAt: string
    updatedAt: string
}

export interface UserAddModel{
    email: string
    password: string
    name: string
    rut: string
}

export interface UserViewModel {
    id: number
    name: string
    rut: string
    email: string
}

export const User = sequelize.define<UserModel>('user', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    email: Sequelize.STRING,
    password: Sequelize.STRING,
    name: Sequelize.STRING,
    rut: Sequelize.STRING,
})