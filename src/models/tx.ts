import * as Sequelize from 'sequelize'
import { sequelize } from '../instances/sequelize'


export interface TxModel extends Sequelize.Model{
    id: number
    type: string
    amount: string
    description: string
    idUser: string
    toUser: string
    createdAt: string
    updatedAt: string
}

export interface TxAddModel{
    type: string
    amount: string
    description: string
    idUser: number
    toUser: number
}

export interface TxViewModel {
    id: number
    type: string
    amount: string
    description: string
    idUser: number
    toUser: number
    createdAt: string
    updatedAt: string
}

export const Tx = sequelize.define<TxModel>('tx', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    type: Sequelize.STRING,
    amount: Sequelize.INTEGER,
    description: Sequelize.STRING,
    idUser: Sequelize.STRING,
    toUser: Sequelize.STRING,


})